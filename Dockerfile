FROM python:3-alpine

RUN apk --update add bash geos gdal libjpeg && \
    apk --update add --virtual .dev build-base geos-dev gdal-dev zlib-dev jpeg-dev && \
    adduser -S -u 1001 -G root app && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    pip3 install requests shapely fiona bokeh && \
    apk del .dev

COPY --chown=1001:0 src /app
COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

RUN pip3 install -e ./app

WORKDIR /app

USER 1001

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["wms_benchmark", "--help"]
