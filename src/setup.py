import setuptools

setuptools.setup(
    name='wms_benchmark',
    version='1.0.0',
    description='WMS Benchmark',
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'wms_benchmark=wms_benchmark:run'
        ]
    }
)