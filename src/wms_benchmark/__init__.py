import argparse
import logging
import os
from wms_benchmark.perimeter import Perimeter
from wms_benchmark.sample import SampleSet
from wms_benchmark.benchmark import Benchmark
from wms_benchmark.report import Report

def run():
    parser = argparse.ArgumentParser(description='WMS Benchmark')

    parser.add_argument(
        '--perimeter',
        type=str,
        dest='perimeter',
        metavar='SHP',
        help='shapefile containing the test perimeter',
        required=True
    )

    parser.add_argument(
        '--wms',
        type=str,
        dest='wms',
        metavar='URL',
        help='URL of a WMS to be called',
        action='append',
        required=True
    )

    parser.add_argument(
        '--wms-version',
        type=str,
        dest='wms_version',
        metavar='STR',
        help='WMS version to be used',
        default='1.1.1'
    )

    parser.add_argument(
        '--image-format',
        type=str,
        dest='image_format',
        metavar='STR',
        help='image format to be used',
        default='image/png'
    )

    parser.add_argument(
        '--crs',
        type=str,
        dest='crs',
        metavar='CRS',
        help='CRS to be used',
        default='EPSG:2056'
    )

    parser.add_argument(
        '--layer',
        type=str,
        dest='layer',
        metavar='LAYER',
        help='layer to be rendered',
        action='append',
        required=True
    )

    parser.add_argument(
        '--combine',
        dest='combine',
        help='run an additional request with all layers',
        action='store_true',
        default=False
    )

    parser.add_argument(
        '--number',
        type=int,
        dest='number',
        metavar='INT',
        help='number of requests',
        default=100
    )

    parser.add_argument(
        '--max-gu',
        type=float,
        dest='max_gu',
        metavar='FLOAT',
        help='maximum width (ground units)',
        default=10000.0
    )

    parser.add_argument(
        '--min-gu',
        type=float,
        dest='min_gu',
        metavar='FLOAT',
        help='minimum width (ground units)',
        default=100.0
    )

    parser.add_argument(
        '--max-px',
        type=int,
        dest='max_px',
        metavar='INT',
        help='maximum width (px)',
        default=256
    )

    parser.add_argument(
        '--min-px',
        type=int,
        dest='min_px',
        metavar='INT',
        help='minimum width (px)',
        default=256
    )

    parser.add_argument(
        '--max-delay',
        type=int,
        dest='max_delay',
        metavar='INT',
        help='maximum delay (ms)',
        default=3000
    )

    parser.add_argument(
        '--min-delay',
        type=int,
        dest='min_delay',
        metavar='INT',
        help='minimum delay (ms)',
        default=0
    )

    parser.add_argument(
        '--ratio',
        type=float,
        dest='ratio',
        metavar='FLOAT',
        help='image ratio',
        default=1.0
    )

    parser.add_argument(
        '--threads',
        type=int,
        dest='threads',
        metavar='INT',
        help='maximum number of parallel threads',
        default=1
    )

    parser.add_argument(
        '--output',
        type=str,
        dest='output',
        metavar='DIR',
        help='output folder',
        required=True
    )

    parser.add_argument(
        '--sample-set',
        type=str,
        dest='sample_set',
        metavar='SHP',
        help='create a shapefile with the generated sample set'
    )

    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s'
    )

    perimeter = Perimeter(args.perimeter)

    sample_set = SampleSet(
        args.number,
        args.max_px,
        args.min_px,
        args.max_gu,
        args.min_gu,
        args.ratio,
        args.max_delay,
        args.min_delay,
        args.sample_set
    )
    sample_set.generate_samples(perimeter.geom)

    benchmark = Benchmark(
        args.wms,
        args.wms_version,
        args.image_format,
        args.crs,
        args.layer,
        args.combine,
        args.threads
    )
    benchmark.run(sample_set.get_samples())
    benchmark.write_results(args.output)

    report = Report(args.wms, args.layer, benchmark.get_results())
    report.write_report(args.output)
    report.create_charts(args.output)
    report.print()
