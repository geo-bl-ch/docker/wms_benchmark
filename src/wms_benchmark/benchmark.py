import logging
import requests
import datetime
import time
import csv
import os
from queue import Queue
import threading


class Benchmark(object):

    __headers = {
        'Cache-Control': 'no-cache'
    }

    def __init__(self, wms, wms_version, image_format, crs, layer, combine, threads):
        self._wms = wms
        self._wms_version = wms_version
        self._image_format = image_format
        self._crs = crs
        self._layer = layer
        self._combine = combine
        self._threads = threads
        if not isinstance(self._wms, list):
            raise AssertionError('wms has to be a list')
        if len(self._wms) < 1:
            raise AssertionError('wms has to contain at least one URL')
        if not isinstance(self._layer, list):
            raise AssertionError('layer has to be a list')
        if len(self._layer) < 1:
            raise AssertionError('layer has to contain at least one layer name')
        if not isinstance(self._combine, bool):
            raise AssertionError('combine has to be a boolean value')
        self._results = []
        self._rid = 0
        self._queue = Queue()
        for i in range(0, self._threads):
            threading.Thread(target=self._worker, daemon=True).start()
            logging.info('created worker thread {0} of {1}'.format(i + 1, self._threads))

    def run(self, sample_set):
        if not isinstance(sample_set, list):
            raise AssertionError('sample set has to be a list')
        if len(sample_set) == 0:
            logging.warning('got empty sample set, skipped benchmark')

        self._results = []
        self._rid = 0

        for wms in self._wms:
            logging.info('starting benchmark for {0}'.format(wms))
            for sample in sample_set:
                for layer in self._layer:
                    self._queue.put({
                        'wms': wms,
                        'layer': layer,
                        'sample': sample
                    })
                    # self._request(wms, layer, sample)
                if self._combine:
                    # self._request(wms, ','.join(self._layer), sample)
                    self._queue.put({
                        'wms': wms,
                        'layer': ','.join(self._layer),
                        'sample': sample
                    })
            self._queue.join()
            logging.info('finished benchmark for {0}'.format(wms))

    def _request(self, wms, layer, sample):
        time.sleep(float(sample['delay']) / 1000.0)
        if self._wms_version == '1.3.0':
            crs_key = 'CRS'
        else:
            crs_key = 'SRS'
        params = {
            'SERVICE': 'WMS',
            'VERSION': self._wms_version,
            'REQUEST': 'GetMap',
            'FORMAT': self._image_format,
            'LAYERS': layer,
            'WIDTH': sample.get('width'),
            'HEIGHT': sample.get('height'),
            'STYLES': '',
            crs_key: self._crs,
            'BBOX': ','.join([str(val) for val in sample['extent'].bounds])
        }
        start = datetime.datetime.now()
        response = requests.get(wms, params=params, headers=self.__headers)
        end = datetime.datetime.now()
        delta = int(round((end - start).total_seconds() * 1000, 0))
        logging.info('delay: {0} ms - delta: {1} ms - status: {2} - url: {3}'.format(
            sample['delay'],
            delta,
            response.status_code,
            response.url
        ))
        if response.headers['content-type'] != self._image_format:
            logging.warning('received content type does not match image format')
        if response.status_code >= 400:
            logging.warning('request failed')
        try:
            layer_id = self._layer.index(layer) + 1
        except ValueError:
            layer_id = len(self._layer) + 1
        self._results.append({
            'id': self._next_result_id(),
            'sample_id': sample['id'],
            'wms_id': self._wms.index(wms) + 1,
            'layer_id': layer_id,
            'url': response.url,
            'status': response.status_code,
            'content-type': response.headers['content-type'],
            'delay': sample['delay'],
            'delta': delta
        })

    def write_results(self, output):
        dest = os.path.join(output, 'raw.csv')
        with open(dest, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            first = True
            for result in self._results:
                if first:
                    writer.writerow(result.keys())
                    first = False
                writer.writerow([result[key] for key in result])
        logging.info('results have been written to {0}'.format(dest))

    def _next_result_id(self):
        self._rid += 1
        return self._rid

    def _worker(self):
        while True:
            item = self._queue.get()
            self._request(item['wms'], item['layer'], item['sample'])
            self._queue.task_done()

    def get_results(self):
        return self._results
