import logging
import fiona
from shapely.geometry import shape, Polygon, MultiPolygon
from shapely.ops import unary_union


class Perimeter(object):
    def __init__(self, shapefile):
        polygons = []
        logging.info('reading {0}'.format(shapefile))
        with fiona.open(shapefile) as shp:
            for feature in shp:
                geom = shape(feature['geometry'])
                if isinstance(geom, Polygon):
                    polygons.append(geom)

                elif isinstance(geom, MultiPolygon):
                    polygons.extend(geom.geoms)
        if len(polygons) == 0:
            raise ValueError('shapefile contains no polygon geometries')
        logging.info('creating perimeter from {0} polygon(s)'.format(len(polygons)))
        self.geom = unary_union(polygons)
