import logging
import statistics
import os
from bokeh.layouts import column
from bokeh.palettes import Spectral6
from bokeh.plotting import figure, output_file, save


class Report(object):
    def __init__(self, wms, layer, results):
        self._report = {
            'wms': {},
            'layer': {}
        }
        self._chart_data = {}
        for result in results:
            wms_id = str(result['wms_id'])
            layer_id = str(result['layer_id'])
            if result['layer_id'] > len(layer):
                layer_name = ','.join(layer)
            else:
                layer_name = layer[result['layer_id'] - 1]
            if wms_id not in self._report['wms']:
                self._report['wms'][wms_id] = {
                    'url': wms[result['wms_id'] - 1],
                    'values': []
                }
            self._report['wms'][wms_id]['values'].append(result['delta'])
            if layer_id not in self._report['layer']:
                self._report['layer'][layer_id] = {
                    'name': layer_name,
                    'values': []
                }
            if layer_id not in self._chart_data:
                self._chart_data[layer_id] = {
                    'name': layer_name,
                    'wms': {}
                }
            if wms_id not in self._chart_data[layer_id]['wms']:
                self._chart_data[layer_id]['wms'][wms_id] = {
                    'url': wms[result['wms_id'] - 1],
                    'values': []
                }
            self._report['layer'][layer_id]['values'].append(result['delta'])
            self._chart_data[layer_id]['wms'][wms_id]['values'].append({
                'x': result['sample_id'],
                'y': result['delta']
            })
        

    def print(self):
        logging.info('{0}{1}'.format(self._generate_header(), self._generate_values()))

    def write_report(self, output):
        dest = os.path.join(output, 'report.txt')
        with open(dest, 'w') as f:
            f.write('{0}{1}'.format(self._generate_header(), self._generate_values()))
        logging.info('report has been written to {0}'.format(dest))

    def create_charts(self, output):
        dest = os.path.join(output, 'charts.html')
        output_file(dest)
        charts = []
        for layer_id in self._chart_data:
            layer = self._chart_data[layer_id]
            p = figure(title=layer['name'], x_axis_label='sample', y_axis_label='duration [ms]')
            idx = 0
            for wms_id in layer['wms']:
                wms = layer['wms'][wms_id]
                sorted_values = sorted(wms['values'], key=lambda k: k['x'])
                p.line(
                    [val['x'] for val in sorted_values],
                    [val['y'] for val in sorted_values],
                    legend_label=wms['url'],
                    line_width=2,
                    line_color=Spectral6[idx]
                )
                idx += 1
            charts.append(p)
        save(column(charts))
        logging.info('charts have been written to {0}'.format(dest))

    def _generate_header(self):
        return """
************************
* WMS Benchmark Report *
************************
        """

    def _generate_values(self):
        values = ''
        for wms_id in self._report['wms']:
            wms = self._report['wms'][wms_id]
            values += """
WMS: {0}
    mean:   {1}
    median: {2}
    max:    {3}
    min:    {4}
    count:  {5}
            """.format(
                wms['url'],
                int(statistics.mean(wms['values'])),
                int(statistics.median(wms['values'])),
                max(wms['values']),
                min(wms['values']),
                len(wms['values'])
            )

        for layer_id in self._report['layer']:
            layer = self._report['layer'][layer_id]
            values += """
Layer: {0}
    mean:   {1}
    median: {2}
    max:    {3}
    min:    {4}
    count:  {5}
            """.format(
                layer['name'],
                int(statistics.mean(layer['values'])),
                int(statistics.median(layer['values'])),
                max(layer['values']),
                min(layer['values']),
                len(layer['values'])
            )
        return values
