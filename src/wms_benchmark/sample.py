import logging
import random
import fiona
from shapely.geometry import Point, Polygon, MultiPolygon, mapping


class SampleSet(object):

    __schema = {
        'geometry': 'Polygon',
        'properties': {'id': 'int'},
    }        

    def __init__(self, size, max_px, min_px, max_gu, min_gu, ratio, max_delay, min_delay, output):
        self._size = size
        self._max_px = max_px
        self._min_px = min_px
        self._max_gu = max_gu
        self._min_gu = min_gu
        self._ratio = ratio
        self._max_delay = max_delay
        self._min_delay = min_delay
        self._output = output
        self._samples = []

    def generate_samples(self, perimeter):
        self._samples = []

        if not isinstance(perimeter, (Polygon, MultiPolygon)):
            raise AssertionError(
                'perimeter has to be an instance of Polygon or MultiPolygon, got {0} instead'
                    .format(type(perimeter))
            )

        extent = perimeter.bounds

        logging.info('perimeter extent: {0}'.format(
            ', '.join([str(val) for val in extent])
        ))

        sid = 1

        while len(self._samples) < self._size:
            x = round(random.uniform(extent[0], extent[2]), 3)
            y = round(random.uniform(extent[1], extent[3]), 3)
            point = Point(x, y)
            if point.within(perimeter):
                width_px = int(random.uniform(self._min_px, self._max_px))
                height_px = int(round(float(width_px) / self._ratio, 0))
                width_gu = random.uniform(self._min_gu, self._max_gu)
                height_gu = width_gu / self._ratio
                min_x = round(point.x - width_gu / 2, 3)
                max_x = round(point.x + width_gu / 2, 3)
                min_y = round(point.y - height_gu / 2, 3)
                max_y = round(point.y + height_gu / 2, 3)
                delay = int(random.uniform(self._min_delay, self._max_delay))
                polygon = Polygon([
                    (min_x, min_y),
                    (min_x, max_y),
                    (max_x, max_y),
                    (max_x, min_y),
                    (min_x, min_y)
                ])
                self._samples.append({
                    'id': sid,
                    'center': point,
                    'extent': polygon,
                    'width': width_px,
                    'height': height_px,
                    'delay': delay
                })
                sid += 1

        logging.info('generated {0} random sample(s) inside the perimeter'.format(
            len(self._samples))
        )

        if self._output is not None:
            with fiona.open(self._output, 'w', 'ESRI Shapefile', self.__schema) as shp:
                for sample in self._samples:
                    shp.write({
                        'geometry': mapping(sample['extent']),
                        'properties': {
                            'id': sample['id']
                        }
                    })
            logging.info('sample set has been written to {0}'.format(self._output))

    def get_samples(self):
        return self._samples
